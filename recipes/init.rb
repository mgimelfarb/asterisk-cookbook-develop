defaults_path = case node['platform_family']
when 'debian'
  '/etc/default/asterisk'
when 'rhel', 'fedora'
  '/etc/sysconfig/asterisk'
end

template defaults_path do
  source 'init/default-asterisk.erb'
  mode 0644
  owner 'root'
  group 'root'
  notifies :enable, 'service[asterisk]'
  notifies :restart, 'service[asterisk]'
end
